# ChatGPT prompt for Experiment № 1

ChatGPT Custom Instructions см. в файле [04_chaos_engineering/chatgpt_prompts/custom_instructions.md](
./custom_instructions.md
).
## RU
```markdown
Напиши план эксперимента с плановым отключением одного из узлов кластера DSS, чтобы проверить процедуру переключения
ролей (failover).

Итоговый план эксперимента должен включать:
1. Описание стратегии по умолчанию выбора DSS нового master-узла на основе технической документации. Укажи референсные значения (бенчмарки) по времени выполнения каждого этапа.
2. Описание аспектов живучести Системы, которые планируется проверить и улучшить по результатам эксперимента.
3. Подробные шаги для создания условий эксперимента.
4. Инструменты и методы для выполнения эксперимента.
5. Ожидаемые результаты. Описание ожидаемого поведения Системы в ответ на условия эксперимента.
6. Способы проверки реальных результатов, включая: фактическое выполнение процедуры выбора DSS нового master-узла;
   время, необходимое для восстановления работоспособности DSS; мониторинг состояний системы до, во время и после
   проведения эксперимента.
```

## EN (для экономии токенов и минимизации «трудностей перевода»)
```markdown
Write an experiment plan with a planned shutdown of one of the nodes of the DSS cluster to test
the role switching procedure (failover).

The final plan of the experiment should include:
1. A description of the default strategy for selecting a new DSS master node based on technical documentation. Specify the reference values (benchmarks) for the execution time of each stage.
2. Description of the aspects of the survivability of the System that are planned to be tested and improved based on the results of the experiment.
3. Detailed steps to create experimental conditions.
4. Tools and methods for performing the experiment.
5. Expected results. Description of the expected behavior of the System in response to experimental conditions.
6. Ways to verify real results, including: the actual execution of the DSS selection procedure for the new master node;
   time required to restore DSS operability; monitoring of system conditions before, during and after conducting an experiment.
```
