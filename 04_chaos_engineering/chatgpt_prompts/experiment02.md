# ChatGPT prompt for Experiment № 2

ChatGPT Custom Instructions см. в файле [04_chaos_engineering/chatgpt_prompts/custom_instructions.md](
./custom_instructions.md
).
## RU
```markdown
Напиши план эксперимента с имитацией частичной потери пакетов или разрыва TCP-соединений между узлами кластера DSS 10.0.10.3/24 и 10.0.10.4/24.

Цель эксперимента — проверить, насколько хорошо Система справляется с временной недоступностью узлов кластера DSS и как
быстро восстанавливается репликация между узлами.

Итоговый план эксперимента должен включать:
1. Описание аспектов живучести Системы, которые планируется проверить и улучшить по результатам эксперимента.
2. Подробные шаги для создания условий эксперимента.
3. Инструменты и методы для выполнения эксперимента.
4. Код команд chaosblade для имитации потери пакетов или разрыва TCP-соединений между узлами.
5. Ожидаемые результаты. Описание ожидаемого поведения Системы в ответ на условия эксперимента.
6. Способы проверки реальных результатов, включая способы мониторинга состояния Системы до, во время и после проведения эксперимента.


```

## EN (для экономии токенов и минимизации «трудностей перевода»)
```markdown
Write an experiment plan to simulate partial packet loss or TCP connection disconnection between DS cluster nodes 10.0.10.3/24 and 10.0.10.4/24.

The purpose of the experiment is to check how well the System copes with the temporary unavailability of DSS cluster nodes and how quickly replication between nodes is restored.

The final plan of the experiment should include:
1. A description of the aspects of the survivability of the System that are planned to be checked and improved based on the results of the experiment.
2. Detailed steps to create experimental conditions.
3. Tools and methods for performing the experiment.
4. chaosblade command code to simulate packet loss or TCP connection disconnection between nodes.
5. Expected results. Description of the expected behavior of the System in response to experimental conditions.
6. Ways to verify the actual results, including ways to monitor the state of the System before, during and after the experiment.
```
