# Experiment Plan for DSS Failover Testing

## 1. Default Strategy for DSS Master Node Selection

`Patroni`, the high-availability cluster manager for `PostgreSQL`, uses a distributed consensus algorithm (typically via
`etcd`, `ZooKeeper` or `Consul`) to decide which node will be the master in the event of a failover.

The default strategy for selecting a new master node in a `PostgreSQL` cluster managed by `Patroni` involves several
steps.
`Patroni` will automatically handle the failover procedure in the event of a failure of the current master node.

The general process is as follows:

- **Detection**: The failure of the master node is detected by other `Patroni` instances via the distributed consensus
  algorithm provided by `etcd`.
- **Leader Election**: A leader election is conducted among the available replicas to choose the new master node.
- **Promotion**: The selected replica is promoted to the master role.
- **Updates**: The configuration is updated across the remaining nodes and the load balancer (e.g., `HAProxy`) to route
  traffic to the new master.

The selection is based on a series of pre-defined rules and health checks that prioritize the most up-to-date and
healthy replicas.

**Reference values (benchmarks)** for each stage may vary depending on the system's hardware, network latency, `Patroni`
configuration, and the size of the dataset. However, a typical failover might complete within 10 to 30 seconds.

[Patroni documentation](https://patroni.readthedocs.io/en/latest/) provides more details on the failover process.

## 2. Aspects of Survivability to be Tested

The experiment will test the following aspects of survivability:

- **Automatic Failover Process**: The ability of the DSS to automatically elect a new master node and redirect traffic
  without manual intervention.
- **Data Integrity**: Ensuring that no data is lost during the failover process.
- **Performance Impact**: Measuring the impact on API response times and system throughput during and after the
  failover.
- **Recovery Time Objective (RTO)**: The time taken for the system to detect the failure, elect a new master, and resume
  normal operations.
- **Client Connection Handling**: Confirming that existing connections are either gracefully terminated or handed off to
  the new master without significant interruption.

## 3. Steps to Create Experimental Conditions

Before starting the experiment, ensure that all steps are reversible, and that data integrity and system operations can
be maintained.

1. Inform stakeholders about the planned experiment, including the time and potential impacts.
2. Ensure all system logs and metrics are being correctly shipped to their respective
   destinations (`Prometheus`, `Grafana`, `Alertmanager` and respective log storage).
3. Ensure a backup plan is in place.
4. Schedule the experiment during a maintenance window or when the impact on users will be minimal.
5. Plan for a backup procedure in case the experiment causes unexpected issues.
6. Use the CloudMTS Cloud Resource Management Console to identify the current master node in the DSS.

## 4. Tools and Methods for the Experiment

- **Locust**: To generate a consistent load on the system's RESTful API and simulate real user traffic.
- **Cloud MTS Console**: To manually shut down one of the DSS nodes to simulate a failure.
- **Grafana and Prometheus**: To monitor the 4 Golden Signals and other relevant metrics before, during, and after the
  experiment.

## 5. Expected Results

Upon shutting down the current master node:

- **Failover**: `Patroni` should detect the failure and initiate a failover to one of the standby nodes within seconds.
- **Data Integrity**: Applications should not experience any data loss due to the failover.
- **Performance**: There may be a brief spike in latency and a temporary drop in throughput during the failover process.
- **Recovery**: The new master should be fully operational, and the system should return to normal performance levels shortly after the failover.

## 6. Verification of Real Results

To verify the real results, the following should be observed and recorded:

- **Failover Execution**: Monitoring logs from `Patroni` and `etcd` to confirm that the failover occurred as
  expected. Use `patronictl list --format=pretty --extended --timestamp -W` to monitor the leader election and the
  promotion of the new master node.
- **Measuring RTO**: Measure the time from node shutdown to the new master being operational using Grafana dashboards.
- **System Logs**: Check system logs for any anomalies during the failover process.
- **Performance Monitoring**: Use Grafana to monitor the 4 Golden Signals before, during, and after the experiment to
  assess the impact on system performance.
- **API Responsiveness**: Continuously make RESTful API requests to the CS and measure responsiveness and error rates.
- **Data Integrity**: Check application logs and database records to confirm that no data was lost or corrupted.

After the experiment, compare the actual results with the expected results to evaluate the success of the failover and
identify any areas for improvement. Document the findings and update the system configuration and procedures as needed
to enhance system survivability.
