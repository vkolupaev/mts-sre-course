# Experiment Plan for Simulating Network Issues in the Data Storage Subsystem (DSS)

## 1. Purpose and Aspects of Survivability

### 1.1 Objective
The objective of this chaos engineering experiment is to evaluate the resilience of the System in the face of network disruptions between the nodes of the DSS. Specifically, we aim to assess:

- The capability of the DSS to handle partial packet loss and TCP disconnections.
- The efficiency of the PostgreSQL cluster's replication and failover mechanisms under adverse network conditions.
- The impact on application performance and availability when the data layer experiences network issues.

### 1.2 Survivability Aspects to Assess
Post-experiment, we plan to investigate the following aspects of the System's survivability to guide improvements:

- Recovery Time Objective (RTO): How quickly the system recovers from failure.
- Recovery Point Objective (RPO): How much data is lost during the disruption.
- The effectiveness of existing monitoring and alerting strategies in detecting and notifying of the event.

## 2. Experiment Preparation Steps

### 2.1 Pre-Experiment Checklist
1. Ensure that all system components are healthy and running as expected.
2. Confirm that backups are up-to-date and recovery procedures are in place.
3. Notify stakeholders of the planned experiment.
4. Validate that the monitoring and logging systems are operational.
5. Secure approval to conduct the test during an appropriate time window to minimize impact.

### 2.2 Test Environment
- Ensure that all tests are carried out in a controlled environment that mirrors production as closely as possible.
- If the experiment is to be performed in production, ensure that measures are in place to minimize potential disruptions to end-users.

## 3. Tools and Methods for the Experiment

- **Load Testing Tool**: Use [Locust](https://locust.io/) to simulate user requests to the RESTful API.
- **Chaos Engineering Tool**: Apply [Chaosblade](https://github.com/chaosblade-io/chaosblade) to induce the desired network conditions.
- **Monitoring Tools**: Utilize Prometheus, Grafana, and Alertmanager to observe the system's behavior and collect relevant metrics.

## 4. Chaosblade Commands

### 4.1 Simulating Packet Loss
```bash
# Command to simulate 30% packet loss for 60 seconds between nodes 10.0.10.3 and 10.0.10.4
/opt/chaosblade-1.7.2/blade create network loss --percent 30 --interface ens160 --destination-ip 10.0.10.4 --timeout 60
```

### 4.2 Simulating TCP Connection Disconnection
```bash
# Command to drop TCP traffic for 60 seconds between nodes 10.0.10.3 and 10.0.10.4
/opt/chaosblade-1.7.2/blade create network drop --source-ip 10.0.10.3 --destination-ip 10.0.10.4 --timeout 60
```

## 5. Expected Results

### 5.1 Packet Loss
- The cluster should trigger failover procedures if the primary node experiences significant packet loss.
- Application performance may degrade, but the overall availability should not be compromised.
- Replication between nodes should resume without data inconsistency once normal network conditions are restored.

### 5.2 TCP Connection Disconnection
- Patroni should detect the disconnection and promote a new primary node if needed.
- The application should be able to continue serving requests, possibly with increased latency or temporary errors during the failover.

## 6. Verification of Results

### 6.1 Monitoring and Observability
- Monitor the DSS using Prometheus and Grafana dashboards throughout the experiment.
- Observe the 4 Golden Signals for abnormalities, and use the logs to understand the sequence of events.

### 6.2 Data Consistency Checks
- After the experiment, perform data integrity checks to ensure no data corruption occurred.

### 6.3 Failover and Recovery Validation
- Verify that failover mechanisms worked as expected and that the RTO and RPO meet the System's objectives.

### 6.4 Post-Experiment Review
- Gather all relevant data and logs from the experiment.
- Conduct a thorough analysis of the System's response to the induced conditions.
- Document lessons learned and recommend improvements to the System's resilience strategy.
