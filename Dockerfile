# ########################################################################################
#  Copyright (c) 2023 Viacheslav Kolupaev; author's website address:
#
#      https://vkolupaev.com/?utm_source=gitlab&utm_medium=link&utm_campaign=copyright
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      https://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ########################################################################################

##########################################################################################
# Dockerfile with instructions for building the Docker image of the application.
#
# Docs:
#    1. Dockerfile reference:
#       https://docs.docker.com/engine/reference/builder/
#    2. Best practices for writing Dockerfiles:
#       https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
#    3. Optimizing builds with cache management:
#       https://docs.docker.com/build/building/cache/
#    4. "docker poetry best practices":
#       https://github.com/python-poetry/poetry/discussions/1879?sort=new
#
##########################################################################################

# Dockerfile syntax definition. Required to mount package manager cache directories.
# See Dockerfile syntax tags here: https://hub.docker.com/r/docker/dockerfile
# Reference: https://docs.docker.com/engine/reference/builder/#syntax
# syntax=docker/dockerfile:1


##########################################################################################
# STAGE 1: PYTHON-BASE-01
#
# Base images need to be updated periodically to the latest stable versions. This is
# required for security reasons.
##########################################################################################
# Build arguments; only before the declaration of the `FROM` instruction. Reference:
# https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact
ARG DOCKER_REGISTRY=docker.io/library
ARG PYTHON_BASE_IMAGE_TAG

# Pull official base image.
# Not the final image, will appear as `<none>:<none>`.
FROM ${DOCKER_REGISTRY}/python:${PYTHON_BASE_IMAGE_TAG} AS python-base-01

# TODO: finish file development
