# Start an example database
# docker run --net=host -it --rm -e POSTGRES_PASSWORD=password postgres
# Connect to it
docker run \
  -d \
  --rm \
  --net=host \
  -e DATA_SOURCE_NAME="postgresql://postgres:postgres-pass@localhost:5432/postgres?sslmode=disable" \
  quay.io/prometheuscommunity/postgres-exporter
