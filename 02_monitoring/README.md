Grafana Dashboards Folder: http://5eca9364-3899-4021-b861-fd4f64e48c6d.mts-gslb.ru/dashboards/f/v6hSDEnSz/kolupaev-viacheslav

Prometheus:
1. config: http://77.105.185.162:9090/config
2. targets: http://77.105.185.162:9090/targets?search=
3. alerts: http://77.105.185.162:9090/alerts?search=

Blackbox exporter:

1. Probes: http://77.105.185.162:9115/probes
2. Metrics: http://77.105.185.162:9115/metrics

App REST API Swagger: http://wheather-service-student43.local/swagger/index.html

Mozilla HTTP response status codes: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
