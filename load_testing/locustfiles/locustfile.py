# ########################################################################################
#  Copyright (c) 2023 Viacheslav Kolupaev; author's website address:
#
#      https://vkolupaev.com/?utm_source=gitlab&utm_medium=link&utm_campaign=copyright
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      https://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ########################################################################################

"""Load testing module using the `locust` library."""

# Standard Library
import logging
import time
from typing import Final
from uuid import uuid4

# Third Party
import gevent
from locust import (
    FastHttpUser,
    env,
    events,
    tag,
    task,
)
from locust.runners import (
    STATE_CLEANUP,
    STATE_STOPPED,
    STATE_STOPPING,
    LocalRunner,
    MasterRunner,
)
from locust.user.wait_time import constant_throughput

max_failure_ratio: Final[int] = 5  # valid values: from 1 to 1000.
max_avg_response_time: Final[int] = 300
response_time_precent: Final[float] = 0.95
response_time_precentile_cutoff: Final[int] = 300


@events.quitting.add_listener
def _(environment: env.Environment, **kw) -> None:
    """Set the exit-code to non-zero.

    Set the exit-code to non-zero if any of the following conditions are met:
        * More than 0.1% of the requests failed.
        * The average response time is longer than 300 ms.
        * The 95th percentile for response time is larger than 250 ms.

    `quitting` — EventHook. Fired after quitting events, just before process is exited.

    Args:
        environment: `locust` Environment instance.

    """
    if environment.stats.total.fail_ratio * 1000 > max_failure_ratio:
        logging.error('Test failed due to failure ratio > {0}%'.format(max_failure_ratio))
        environment.process_exit_code = 1
    elif environment.stats.total.avg_response_time > max_avg_response_time:
        logging.error('Test failed due to average response time > {0} ms'.format(max_avg_response_time))
        environment.process_exit_code = 1
    elif environment.stats.total.get_response_time_percentile(response_time_precent) > response_time_precentile_cutoff:
        logging.error(
            'Test failed due to {0}th percentile response time > {1} ms'.format(
                int(response_time_precent * 100),
                response_time_precentile_cutoff,
            ),
        )
        environment.process_exit_code = 1
    else:
        environment.process_exit_code = 0


def checker(environment: env.Environment, **kw) -> None:
    """Check test force termination conditions.

    Quit the test if any of the following conditions are met:
        * More than 0.1% of the requests failed.
        * The average response time is longer than 300 ms.
        * The 95th percentile for response time is larger than 250 ms.

    Args:
        environment: `locust` Environment instance.

    """
    while not environment.runner.state in [STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP]:
        time.sleep(1)
        if environment.runner.stats.total.fail_ratio * 1000 > max_failure_ratio:
            logging.error('Quitting due to failure ratio > {0}%'.format(max_failure_ratio))
            environment.runner.quit()
            return
        elif (environment.runner.stats.total.get_response_time_percentile(response_time_precent) > response_time_precentile_cutoff):
            logging.error(
                'Quitting due to {0}th percentile response time > {1} ms'.format(
                    int(response_time_precent * 100),
                    response_time_precentile_cutoff,
                ),
            )
            environment.runner.quit()
            return


@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    # don't run this on workers, we only care about the aggregated numbers
    if isinstance(environment.runner, MasterRunner) or isinstance(environment.runner, LocalRunner):
        gevent.spawn(checker, environment)


class RestfulApiUser(FastHttpUser):
    wait_time = constant_throughput(1)

    @tag('rest_api')
    @task(weight=1)
    def test_performance_b2c_profile(self):
        """Test the performance of some REST API endpoint."""
        idempotency_key = uuid4()
        # FastHttpSession class: https://docs.locust.io/en/stable/increase-performance.html#fasthttpsession-class
        with self.rest(
            method="GET",
            url='/Forecast',
            headers={
                'Host': 'wheather-service-student43.local:80',
                'idempotency-key': idempotency_key,
            },
            # debug_stream=sys.stderr,
        ) as _resp:
            pass
