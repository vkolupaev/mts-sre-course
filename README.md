🚀 mts-sre-course
=======

![GitLab License](https://img.shields.io/gitlab/license/vkolupaev/notebook?color=informational)
![Ansible](https://img.shields.io/static/v1?label=Ansible&message=2.15.4&color=informational&logo=ansible&logoColor=white)
![Helm](https://img.shields.io/static/v1?label=Helm&message=3.13&color=informational&logo=helm&logoColor=white)
![Python](https://img.shields.io/static/v1?label=Python&message=3.11&color=informational&logo=python&logoColor=white)
![yamllint](https://img.shields.io/static/v1?label=yamllint&message=checked&color=informational&logo=yaml&logoColor=white)

---

[[_TOC_]]

## 👋 What is this repository?
This is a repository with the result of completing homework for module No. 2 as part of
the course "SRE: Strategy and methods" from MTS.Teta.

![High-Level Design](./docs/img/high-level-design.png "High-Level Design")

## ✅ What is done
1. [x] Helm Chart was developed, see `./helm_charts/mts-sre-course/templates`.
2. [x] Helm Chart is deployed in the provided Kubernetes namespace.
3. [x] In the Cloud MTS, 6 virtual machines are pre-configured according to the HLD.
4. [x] Modified the "PostgreSQL High-Availability Cluster" template for project purposes:
   1. The `inventory` file has been modified.
   2. Added the `postgresql-tables` role, see `./postgresql_cluster/roles/postgresql-tables/tasks/main.yml`.
   3. The `deploy_pgcluster.yml` and `config_cluster.yml` files have been modified: the `postgresql-tables` role has
      been added.
   4. Modified file `./postgresql_cluster/vars/main.yml`:
      1. the `with_haproxy_load_balancing` option is enabled;
      2. the `pgbouncer_install` option is disabled;
      3. modified the `postgresql_databases` option → now the `weather` database is created when the setup is deployed;
      4. added the `postgresql_tables` option to automatically create database tables when deploying the setup;
      5. the `postgresql_pg_hba` option has been modified to automatically configure `pg_hba.conf` when deploying
         the setup.
5. [x] The modified setup "PostgreSQL High-Availability Cluster" is deployed to virtual machines using Ansible.
6. [x] HTTP requests have been developed to check the health of the project from outside the setup, see
   `http_requests/project_health_check_requests.http`.
7. [x] The health check of the setup was performed using HTTP requests made from the outside.
8. [x] Developed this `README.md`.

🔗 Application Links:
1. [Swagger: http://wheather-service-student43.local/swagger/index.html](
   http://wheather-service-student43.local/swagger/index.html
   ).
2. [HAProxy statistics report](http://77.105.185.162:7000/stat).

## 👨‍🎓 Student Information
1. CloudMTS UID: `6eee5bce-a583-4b47-825c-c56d0d79c1c3`;
2. CloudMTS Project ID: `156689e8-3048-4961-b789-87ba0361cf48`;
3. Kubernetes namespace info:
    ```yaml
    contexts:
      - name: student43@grand-lion-d99896
        context:
          cluster: grand-lion-d99896
          namespace: sre-cource-student-43
          user: student43
    ```
4. e-mail: [v.s.kolupaev@gmail.com](mailto:v.s.kolupaev@gmail.com).

## ⚙ Resources and Documentation

### Resources
⚠ Warning! Authorization may be required to access some resources

1. [Course page](https://teta.mts.ru/members/courses/course814873634518/vvodnyj-modul-250871868273).
2. [Homework page](
   https://teta.mts.ru/members/courses/course814873634518/domasnee-zadanie-po-modulu-2-342260641269
   ).
3. Lectures:
   1. [How to build reliable services on top of less reliable infrastructure](
      https://www.youtube.com/watch?v=kj5MCxOZHO8
      ).
   2. [Master class on DevOps](https://www.youtube.com/watch?v=DJnSH0fRWUU).
4. [#CloudMTS](https://hub.cloud.mts.ru/).
5. [GitHub: PostgreSQL High-Availability Cluster](https://github.com/vitabaks/postgresql_cluster).
6. [Repository with Application Docker Image](
   https://github.com/users/ldest/packages/container/package/sre-course%2Fapi
   ).

### Documentation
1. [Ansible](https://docs.ansible.com/ansible/latest/index.html).
2. [Helm](https://helm.sh/docs/).
3. [Kubernetes](https://kubernetes.io/docs/home/).
4. [yamllint](https://yamllint.readthedocs.io/en/stable/).
5. [The Raft Consensus Algorithm](https://raft.github.io/).
6. [PostgreSQL connection strings](https://www.connectionstrings.com/postgresql/)

## I have an error or have a question or idea💡. What to do?
Please feel free to write to me 💬. My contacts are listed below 👇.

---
Copyright 2023. [Viacheslav Kolupaev](
https://vkolupaev.com/?utm_source=gitlab&utm_medium=link&utm_campaign=mts-sre-course
).

[![website](
https://img.shields.io/static/v1?label=website&message=vkolupaev.com&color=blueviolet&style=for-the-badge&
)](https://vkolupaev.com/?utm_source=gitlab&utm_medium=badge&utm_campaign=mts-sre-course)

[![LinkedIn](
https://img.shields.io/static/v1?label=LinkedIn&message=vkolupaev&color=informational&style=flat&logo=linkedin
)](https://www.linkedin.com/in/vkolupaev/)
[![Telegram](
https://img.shields.io/static/v1?label=Telegram&message=@vkolupaev&color=informational&style=flat&logo=telegram
)](https://t.me/vkolupaev/)
